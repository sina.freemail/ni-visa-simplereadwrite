Imports System.Windows.Forms
Imports NationalInstruments.Visa

Namespace NationalInstruments.Examples.SimpleReadWrite
    Public Class SelectResource
        Inherits System.Windows.Forms.Form

        Private okButton As System.Windows.Forms.Button
        Private closeButton As System.Windows.Forms.Button
        Private visaResourceNameTextBox As System.Windows.Forms.TextBox
        Private AvailableResourcesLabel As System.Windows.Forms.Label
        Private ResourceStringLabel As System.Windows.Forms.Label
        Friend WithEvents availableResourcesListBox As System.Windows.Forms.ListBox
        Private components As System.ComponentModel.Container = Nothing

        Public Sub New()
            InitializeComponent()
        End Sub

        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then

                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If

            MyBase.Dispose(disposing)
        End Sub

        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SelectResource))
            Me.okButton = New System.Windows.Forms.Button()
            Me.closeButton = New System.Windows.Forms.Button()
            Me.visaResourceNameTextBox = New System.Windows.Forms.TextBox()
            Me.AvailableResourcesLabel = New System.Windows.Forms.Label()
            Me.ResourceStringLabel = New System.Windows.Forms.Label()
            Me.availableResourcesListBox = New System.Windows.Forms.ListBox()
            Me.SuspendLayout()
            '
            'okButton
            '
            Me.okButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
            Me.okButton.DialogResult = System.Windows.Forms.DialogResult.OK
            Me.okButton.Location = New System.Drawing.Point(5, 190)
            Me.okButton.Name = "okButton"
            Me.okButton.Size = New System.Drawing.Size(77, 23)
            Me.okButton.TabIndex = 2
            Me.okButton.Text = "OK"
            '
            'closeButton
            '
            Me.closeButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
            Me.closeButton.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.closeButton.Location = New System.Drawing.Point(82, 190)
            Me.closeButton.Name = "closeButton"
            Me.closeButton.Size = New System.Drawing.Size(77, 23)
            Me.closeButton.TabIndex = 3
            Me.closeButton.Text = "Cancel"
            '
            'visaResourceNameTextBox
            '
            Me.visaResourceNameTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.visaResourceNameTextBox.Location = New System.Drawing.Point(5, 162)
            Me.visaResourceNameTextBox.Name = "visaResourceNameTextBox"
            Me.visaResourceNameTextBox.Size = New System.Drawing.Size(282, 19)
            Me.visaResourceNameTextBox.TabIndex = 4
            Me.visaResourceNameTextBox.Text = "GPIB0::2::INSTR"
            '
            'AvailableResourcesLabel
            '
            Me.AvailableResourcesLabel.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.AvailableResourcesLabel.Location = New System.Drawing.Point(5, 5)
            Me.AvailableResourcesLabel.Name = "AvailableResourcesLabel"
            Me.AvailableResourcesLabel.Size = New System.Drawing.Size(279, 11)
            Me.AvailableResourcesLabel.TabIndex = 5
            Me.AvailableResourcesLabel.Text = "Available Resources:"
            '
            'ResourceStringLabel
            '
            Me.ResourceStringLabel.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.ResourceStringLabel.Location = New System.Drawing.Point(5, 147)
            Me.ResourceStringLabel.Name = "ResourceStringLabel"
            Me.ResourceStringLabel.Size = New System.Drawing.Size(279, 12)
            Me.ResourceStringLabel.TabIndex = 6
            Me.ResourceStringLabel.Text = "Resource String:"
            '
            'availableResourcesListBox
            '
            Me.availableResourcesListBox.FormattingEnabled = True
            Me.availableResourcesListBox.ItemHeight = 12
            Me.availableResourcesListBox.Location = New System.Drawing.Point(7, 20)
            Me.availableResourcesListBox.Name = "availableResourcesListBox"
            Me.availableResourcesListBox.Size = New System.Drawing.Size(277, 124)
            Me.availableResourcesListBox.TabIndex = 7
            '
            'SelectResource
            '
            Me.AcceptButton = Me.okButton
            Me.AutoScaleBaseSize = New System.Drawing.Size(5, 12)
            Me.CancelButton = Me.closeButton
            Me.ClientSize = New System.Drawing.Size(292, 220)
            Me.Controls.Add(Me.availableResourcesListBox)
            Me.Controls.Add(Me.ResourceStringLabel)
            Me.Controls.Add(Me.AvailableResourcesLabel)
            Me.Controls.Add(Me.visaResourceNameTextBox)
            Me.Controls.Add(Me.closeButton)
            Me.Controls.Add(Me.okButton)
            Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
            Me.KeyPreview = True
            Me.MaximizeBox = False
            Me.MinimumSize = New System.Drawing.Size(177, 228)
            Me.Name = "SelectResource"
            Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
            Me.Text = "Select Resource"
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub

        Private Sub OnLoad(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Using rmSession = New ResourceManager()
                Dim resources = rmSession.Find("(ASRL|GPIB|TCPIP|USB)?*")

                For Each s As String In resources
                    availableResourcesListBox.Items.Add(s)
                Next
            End Using
        End Sub





        Public Property ResourceName As String
            Get
                Return visaResourceNameTextBox.Text
            End Get
            Set(ByVal value As String)
                visaResourceNameTextBox.Text = value
            End Set
        End Property

        Private Sub availableResourcesListBox_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles availableResourcesListBox.SelectedIndexChanged
            Dim selectedString As String = CStr(availableResourcesListBox.SelectedItem)
            ResourceName = selectedString
        End Sub

        Private Sub availableResourcesListBox_DoubleClick(sender As System.Object, e As System.EventArgs) Handles availableResourcesListBox.DoubleClick
            Dim selectedString As String = CStr(availableResourcesListBox.SelectedItem)
            ResourceName = selectedString
            Me.DialogResult = DialogResult.OK
            Me.Close()
        End Sub
    End Class
End Namespace
