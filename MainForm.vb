Imports System
Imports System.Windows.Forms
Imports NationalInstruments.Visa

Namespace NationalInstruments.Examples.SimpleReadWrite
    Public Class MainForm
        Inherits System.Windows.Forms.Form

        Private mbSession As MessageBasedSession
        Private lastResourceString As String = Nothing
        Private writeTextBox As System.Windows.Forms.TextBox
        Private readTextBox As System.Windows.Forms.TextBox
        Private stringToWriteLabel As System.Windows.Forms.Label
        Private stringToReadLabel As System.Windows.Forms.Label
        Friend WithEvents openSession As System.Windows.Forms.Button
        Friend WithEvents closeSession As System.Windows.Forms.Button
        Friend WithEvents QueryButton As System.Windows.Forms.Button
        Friend WithEvents Write As System.Windows.Forms.Button
        Friend WithEvents Read As System.Windows.Forms.Button
        Friend WithEvents ClearButton As System.Windows.Forms.Button
        Private components As System.ComponentModel.Container = Nothing

        Public Sub New()
            InitializeComponent()
            SetupControlState(False)
        End Sub

        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then

                If mbSession IsNot Nothing Then
                    mbSession.Dispose()
                End If

                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If

            MyBase.Dispose(disposing)
        End Sub

        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MainForm))
            Me.writeTextBox = New System.Windows.Forms.TextBox()
            Me.readTextBox = New System.Windows.Forms.TextBox()
            Me.stringToWriteLabel = New System.Windows.Forms.Label()
            Me.stringToReadLabel = New System.Windows.Forms.Label()
            Me.openSession = New System.Windows.Forms.Button()
            Me.closeSession = New System.Windows.Forms.Button()
            Me.QueryButton = New System.Windows.Forms.Button()
            Me.Write = New System.Windows.Forms.Button()
            Me.Read = New System.Windows.Forms.Button()
            Me.ClearButton = New System.Windows.Forms.Button()
            Me.SuspendLayout()
            '
            'writeTextBox
            '
            Me.writeTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.writeTextBox.Location = New System.Drawing.Point(5, 50)
            Me.writeTextBox.Name = "writeTextBox"
            Me.writeTextBox.Size = New System.Drawing.Size(275, 19)
            Me.writeTextBox.TabIndex = 2
            Me.writeTextBox.Text = "*IDN?\n"
            '
            'readTextBox
            '
            Me.readTextBox.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.readTextBox.Location = New System.Drawing.Point(5, 126)
            Me.readTextBox.Multiline = True
            Me.readTextBox.Name = "readTextBox"
            Me.readTextBox.ReadOnly = True
            Me.readTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
            Me.readTextBox.Size = New System.Drawing.Size(275, 131)
            Me.readTextBox.TabIndex = 6
            Me.readTextBox.TabStop = False
            '
            'stringToWriteLabel
            '
            Me.stringToWriteLabel.Location = New System.Drawing.Point(5, 31)
            Me.stringToWriteLabel.Name = "stringToWriteLabel"
            Me.stringToWriteLabel.Size = New System.Drawing.Size(91, 13)
            Me.stringToWriteLabel.TabIndex = 8
            Me.stringToWriteLabel.Text = "String to Write:"
            '
            'stringToReadLabel
            '
            Me.stringToReadLabel.Location = New System.Drawing.Point(5, 108)
            Me.stringToReadLabel.Name = "stringToReadLabel"
            Me.stringToReadLabel.Size = New System.Drawing.Size(101, 14)
            Me.stringToReadLabel.TabIndex = 9
            Me.stringToReadLabel.Text = "String Read:"
            '
            'openSession
            '
            Me.openSession.Location = New System.Drawing.Point(4, 2)
            Me.openSession.Name = "openSession"
            Me.openSession.Size = New System.Drawing.Size(92, 23)
            Me.openSession.TabIndex = 10
            Me.openSession.Text = "Open Session"
            Me.openSession.UseVisualStyleBackColor = True
            '
            'closeSession
            '
            Me.closeSession.Location = New System.Drawing.Point(102, 2)
            Me.closeSession.Name = "closeSession"
            Me.closeSession.Size = New System.Drawing.Size(75, 23)
            Me.closeSession.TabIndex = 11
            Me.closeSession.Text = "Close Session"
            Me.closeSession.UseVisualStyleBackColor = True
            '
            'QueryButton
            '
            Me.QueryButton.Location = New System.Drawing.Point(4, 75)
            Me.QueryButton.Name = "QueryButton"
            Me.QueryButton.Size = New System.Drawing.Size(75, 23)
            Me.QueryButton.TabIndex = 12
            Me.QueryButton.Text = "Query"
            Me.QueryButton.UseVisualStyleBackColor = True
            '
            'Write
            '
            Me.Write.Location = New System.Drawing.Point(85, 75)
            Me.Write.Name = "Write"
            Me.Write.Size = New System.Drawing.Size(75, 23)
            Me.Write.TabIndex = 13
            Me.Write.Text = "Write"
            Me.Write.UseVisualStyleBackColor = True
            '
            'Read
            '
            Me.Read.Location = New System.Drawing.Point(166, 75)
            Me.Read.Name = "Read"
            Me.Read.Size = New System.Drawing.Size(75, 23)
            Me.Read.TabIndex = 14
            Me.Read.Text = "Read"
            Me.Read.UseVisualStyleBackColor = True
            '
            'ClearButton
            '
            Me.ClearButton.Location = New System.Drawing.Point(13, 264)
            Me.ClearButton.Name = "ClearButton"
            Me.ClearButton.Size = New System.Drawing.Size(267, 23)
            Me.ClearButton.TabIndex = 15
            Me.ClearButton.Text = "Clear"
            Me.ClearButton.UseVisualStyleBackColor = True
            '
            'MainForm
            '
            Me.AutoScaleBaseSize = New System.Drawing.Size(5, 12)
            Me.ClientSize = New System.Drawing.Size(287, 289)
            Me.Controls.Add(Me.ClearButton)
            Me.Controls.Add(Me.Read)
            Me.Controls.Add(Me.Write)
            Me.Controls.Add(Me.QueryButton)
            Me.Controls.Add(Me.closeSession)
            Me.Controls.Add(Me.openSession)
            Me.Controls.Add(Me.stringToReadLabel)
            Me.Controls.Add(Me.stringToWriteLabel)
            Me.Controls.Add(Me.readTextBox)
            Me.Controls.Add(Me.writeTextBox)
            Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
            Me.MaximizeBox = False
            Me.MinimumSize = New System.Drawing.Size(295, 292)
            Me.Name = "MainForm"
            Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
            Me.Text = "Simple Read/Write"
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub

        <STAThread()>
        Private Shared Sub Main()
            Application.Run(New MainForm())
        End Sub










        Private Sub SetupControlState(ByVal isSessionOpen As Boolean)
            openSession.Enabled = Not isSessionOpen
            closeSession.Enabled = isSessionOpen
            QueryButton.Enabled = isSessionOpen
            Write.Enabled = isSessionOpen
            Read.Enabled = isSessionOpen
            writeTextBox.Enabled = isSessionOpen
            clearButton.Enabled = isSessionOpen

            If isSessionOpen Then
                readTextBox.Text = String.Empty
                writeTextBox.Focus()
            End If
        End Sub

        Private Function ReplaceCommonEscapeSequences(ByVal s As String) As String
            Return s.Replace("\n", vbLf).Replace("\r", vbCr)
        End Function

        Private Function InsertCommonEscapeSequences(ByVal s As String) As String
            Return s.Replace(vbLf, "\n").Replace(vbCr, "\r")
        End Function






        Private Sub openSession_Click(sender As System.Object, e As System.EventArgs) Handles openSession.Click
            Using sr As SelectResource = New SelectResource()

                If lastResourceString IsNot Nothing Then
                    sr.ResourceName = lastResourceString
                End If

                Dim result As DialogResult = sr.ShowDialog(Me)

                If result = DialogResult.OK Then
                    lastResourceString = sr.ResourceName
                    Cursor.Current = Cursors.WaitCursor

                    Using rmSession = New ResourceManager()

                        Try
                            mbSession = CType(rmSession.Open(sr.ResourceName), MessageBasedSession)
                            SetupControlState(True)
                        Catch __unusedInvalidCastException1__ As InvalidCastException
                            MessageBox.Show("Resource selected must be a message-based session")
                        Catch exp As Exception
                            MessageBox.Show(exp.Message)
                        Finally
                            Cursor.Current = Cursors.[Default]
                        End Try
                    End Using
                End If
            End Using
        End Sub

        Private Sub closeSession_Click(sender As System.Object, e As System.EventArgs) Handles closeSession.Click
            SetupControlState(False)
            mbSession.Dispose()
        End Sub


        Private Sub QueryButton_Click(sender As System.Object, e As System.EventArgs) Handles QueryButton.Click
            readTextBox.Text = String.Empty
            Cursor.Current = Cursors.WaitCursor

            Try
                Dim textToWrite As String = ReplaceCommonEscapeSequences(writeTextBox.Text)
                mbSession.RawIO.Write(textToWrite)
                readTextBox.Text = InsertCommonEscapeSequences(mbSession.RawIO.ReadString())
            Catch exp As Exception
                MessageBox.Show(exp.Message)
            Finally
                Cursor.Current = Cursors.[Default]
            End Try
        End Sub

        Private Sub Write_Click(sender As System.Object, e As System.EventArgs) Handles Write.Click
            Try
                Dim textToWrite As String = ReplaceCommonEscapeSequences(writeTextBox.Text)
                mbSession.RawIO.Write(textToWrite)
            Catch exp As Exception
                MessageBox.Show(exp.Message)
            End Try
        End Sub

        Private Sub Read_Click(sender As System.Object, e As System.EventArgs) Handles Read.Click
            readTextBox.Text = String.Empty
            Cursor.Current = Cursors.WaitCursor

            Try
                readTextBox.Text = InsertCommonEscapeSequences(mbSession.RawIO.ReadString())
            Catch exp As Exception
                MessageBox.Show(exp.Message)
            Finally
                Cursor.Current = Cursors.[Default]
            End Try
        End Sub

        Private Sub ClearButton_Click(sender As System.Object, e As System.EventArgs) Handles ClearButton.Click
            readTextBox.Text = String.Empty
        End Sub






    End Class
End Namespace
